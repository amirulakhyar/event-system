<?php
session_start();

if (!isset($_SESSION['scan2goA'])) {
  $_SESSION['msg'] = "You must log in first";
  header('location: login.php');
}

if (isset($_GET['logout'])) {
  session_destroy();
  unset($_SESSION['scan2goA']);
  header("location: login.php");
}

include('../database.php');
$id = $_GET['id'];
$sql = "SELECT `participant_event_id` FROM `participant` WHERE `participant_id`='$id'";
$result = $db->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $event_id=$row["participant_event_id"];
    }
}
$sql = "DELETE FROM `participant` WHERE `participant_id` ='$id'";
$db->query($sql);
header("location:../eventmanagment.php?id=".$event_id);
 ?>
