-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 29, 2020 at 12:05 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `poli_azam`
--

-- --------------------------------------------------------

--
-- Table structure for table `ajk`
--

CREATE TABLE `ajk` (
  `ajk_id` int(11) NOT NULL,
  `ajk_name` varchar(100) NOT NULL,
  `ajk_username` varchar(100) NOT NULL,
  `ajk_pass` varchar(100) NOT NULL,
  `ajk_event_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ajk`
--

INSERT INTO `ajk` (`ajk_id`, `ajk_name`, `ajk_username`, `ajk_pass`, `ajk_event_id`) VALUES
(2, 'WONG', 'wong', 'ad99e89bf5e742fe8f58e303c72beeec', '4');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `event_title` varchar(100) NOT NULL,
  `event_date` varchar(100) NOT NULL,
  `event_time` varchar(100) NOT NULL,
  `event_author` varchar(100) NOT NULL,
  `event_author_no` varchar(100) NOT NULL,
  `event_memo` varchar(100) NOT NULL,
  `event_info` varchar(100) NOT NULL,
  `event_banner` varchar(100) NOT NULL,
  `event_status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_title`, `event_date`, `event_time`, `event_author`, `event_author_no`, `event_memo`, `event_info`, `event_banner`, `event_status`) VALUES
(1, 'PENCUCI TANDAS EVENT', '2020-02-15', '13:00', 'Amirul Akhyar', '', '', 'no Info', '', '1'),
(2, 'EVENT LOL', '2020-02-15', '13:00', 'AMIRUL AKHYAR', '', '', ' ', '29022020433506370Devops-840x300.jpg', '1'),
(3, 'LARIAN MERENTAS DESA', '2020-02-14', '01:59', 'amirul akhyar', '', '', '017 -2721840 ', '', '1'),
(4, 'HARI SUKAN MALAYSIA', '2020-02-29', '01:00', 'AMIRUL AKHYAR', '', '', '0172721840 ', '29022020253213353161-1613570_devops-seguridad-trend-micro-devops-engineer.jpg', '1'),
(5, 'WOI', '02-05-2020', '14:00', 'HAIKAL HARIZ', '', '', ' wow', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `participant_id` int(11) NOT NULL,
  `participant_name` varchar(100) NOT NULL,
  `participant_ic` varchar(100) NOT NULL,
  `participant_matick` varchar(100) NOT NULL,
  `participant_no` varchar(100) NOT NULL,
  `participant_code` varchar(100) NOT NULL,
  `participant_event_id` varchar(100) NOT NULL,
  `participant_attandance` varchar(100) NOT NULL,
  `participant_attandance_time` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`participant_id`, `participant_name`, `participant_ic`, `participant_matick`, `participant_no`, `participant_code`, `participant_event_id`, `participant_attandance`, `participant_attandance_time`) VALUES
(8, 'MUHD AZAM', '970817666211', 'ABS12131', '0193214264', '4189', '4', '1', '02:24:pm');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(1, 'amirulakhyar', 'amirulakhyar97@gmail.com', 'd193af2039a8aed91305461dd8ce7a41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ajk`
--
ALTER TABLE `ajk`
  ADD PRIMARY KEY (`ajk_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`participant_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ajk`
--
ALTER TABLE `ajk`
  MODIFY `ajk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `participant`
--
ALTER TABLE `participant`
  MODIFY `participant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
