<?php
	session_start();

	if (!isset($_SESSION['scan2goA'])) {
		$_SESSION['msg'] = "You must log in first";
		header('location: login.php');
	}

	if (isset($_GET['logout'])) {
		session_destroy();
		unset($_SESSION['scan2goA']);
		header("location: login.php");
	}

?>
<!doctype html>
<html lang="en">

<head>
		<title>SCAN2 GO SYSTEM</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/vendor/themify-icons/css/themify-icons.css">
		<link rel="stylesheet" href="assets/vendor/pace/themes/orange/pace-theme-minimal.css">
		<link rel="stylesheet" href="assets/vendor/datatables/css-main/jquery.dataTables.min.css">
		<link rel="stylesheet" href="assets/vendor/datatables/css-bootstrap/dataTables.bootstrap.min.css">
		<link rel="stylesheet" href="assets/vendor/datatables-tabletools/css/dataTables.tableTools.css">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="assets/css/main.min.css">
		<link rel="stylesheet" href="assets/css/skins/sidebar-nav-darkgray.css" type="text/css">
		<link rel="stylesheet" href="assets/css/skins/navbar3.css" type="text/css">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	</head>
	<body>
		<!-- WRAPPER -->
		<div id="wrapper">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="brand">
					<a href="index.php">
						<img src="assets/img/logo-white.png" alt="Logo" class="img-responsive logo" width="150px;" style="margin-top:-15px;">
					</a>
				</div>
				<div class="container-fluid">
					<div id="tour-fullwidth" class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="ti-arrow-circle-left"></i></button>
					</div>
					<form class="navbar-form navbar-left search-form">
						<input type="text" value="" class="form-control" placeholder="Search dashboard...">
						<button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
					</form>
					<div id="navbar-menu">
						<ul class="nav navbar-nav navbar-right">

							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="assets/img/user.png" alt="Avatar">
									<span>Samuel</span>
								</a>
								<ul class="dropdown-menu logged-user-menu">
									<li><a href="#"><i class="ti-user"></i> <span>My Profile</span></a></li>
									<li><a href="appviews-inbox.html"><i class="ti-email"></i> <span>Message</span></a></li>
									<li><a href="#"><i class="ti-settings"></i> <span>Settings</span></a></li>
									<li><a href="page-lockscreen.html"><i class="ti-power-off"></i> <span>Logout</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- LEFT SIDEBAR -->
			<div id="sidebar-nav" class="sidebar">
				<nav>
					<ul class="nav" id="sidebar-nav-menu">
						<li class="menu-group">Main</li>
						<li><a href="index.php"><i class="ti-dashboard"></i> <span class="title">Dashboard</span></a></li>
						<li><a href="event.php"><i class="ti-crown"></i> <span class="title">Event</span></a></li>
						<li><a href="report.php"><i class="ti-zip"></i> <span class="title">Report</span></a></li>
						<li><a href="setting.php"><i class="ti-settings"></i> <span class="title">Setting</span></a></li>
						<li><a href="logout.php"><i class="ti-share-alt"></i> <span class="title">Sign Out</span></a></li>
					</ul>
					<button type="button" class="btn-toggle-minified" title="Toggle Minified Menu"><i class="ti-arrows-horizontal"></i></button>
				</nav>
			</div>
			<!-- END LEFT SIDEBAR -->
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">EVENT MANAGMENT</h1>
							<p class="page-subtitle">SCAN2 GO</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
							<li class="active">EVENT MANAGMENT</li>
						</ul>
					</div>
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">EVENT LIST</h3>
									</div>
									<div class="panel-body">
												<table id="datatable-basic-scrolling" class="table table-sorting table-hover datatable">
													<thead>
														<tr>
															<th>Title</th>
															<th>Date</th>
															<th>Time</th>
															<th>Author</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<?php
														include('database.php');
														$sql = "SELECT * FROM `event`";
														$result = $db->query($sql);
														if ($result->num_rows > 0) {
														    while($row = $result->fetch_assoc()) {
														     echo"
																 <tr>
		 															<td>".$row['event_title']."</td>
		 															<td>".$row['event_date']."</td>
		 															<td>".$row['event_time']."</td>
		 															<td>".$row['event_author']."</td>
		 															<td><a href='eventmanagment.php?id=".$row['event_id']."'><button type='button' class='btn btn-warning'>Edit</button><a></td>
		 														</tr>

																 ";
														    }
														}

														 ?>

													</tbody>
												</table>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel">
										<div class="panel-heading">
											<h3 class="panel-title">CREATE EVENT</h3>
										</div>
										<div class="panel-body">
											<form class="form-horizontal" autocomplete="off" action="action/create_event.php" method="post">
												<div class="form-group">
													<label class="col-sm-2 control-label">Title :</label>
													<div class="col-sm-10">
														<input type="text" onkeyup="this.value = this.value.toUpperCase();" class="form-control" name="event_title" required>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label">Date :</label>
													<div class="col-sm-10">
														<input type="date" class="form-control" name="event_date" required>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label">Time :</label>
													<div class="col-sm-10">
														<input type="time" class="form-control" name="event_time" required>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label">Author :</label>
													<div class="col-sm-10">
														<input type="text" onkeyup="this.value = this.value.toUpperCase();" class="form-control" name="event_author" required>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label">Info :</label>
													<div class="col-sm-10">
														<textarea class="form-control"name="event_info" required> </textarea>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label"></label>
													<div class="col-sm-10">
														<button type="submit" class="btn btn-primary btn-sm btn-block">CREATE EVENT</button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>



							</div>
						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; 2020 <a href="" target="_blank">SCAN2GO	</a></p>
				</div>
			</footer>
		</div>
		<!-- END WRAPPER -->
		<!-- Javascript -->
		<script src="assets/vendor/jquery/jquery.min.js"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/vendor/pace/pace.min.js"></script>
		<script src="assets/scripts/klorofilpro-common.min.js"></script>
		<script src="assets/vendor/datatables/js-main/jquery.dataTables.min.js"></script>
		<script src="assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js"></script>
		<script src="assets/vendor/datatables-colreorder/dataTables.colReorder.js"></script>
		<script src="assets/vendor/datatables-tabletools/js/dataTables.tableTools.js"></script>
		<script>
		$(function()
		{
			// datatable column with reorder extension
			$('#datatable-column-reorder').dataTable(
			{
				pagingType: "full_numbers",
				sDom: "RC" +
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				colReorder: true,
			});
			// datatable with column filter enabled
			var dtTable = $('#datatable-column-filter').DataTable(
			{ // use DataTable, not dataTable
				sDom: // redefine sDom without lengthChange and default search box
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>"
			});
			$('#datatable-column-filter thead').append('<tr class="row-filter"><th></th><th></th><th></th><th></th><th></th></tr>');
			$('#datatable-column-filter thead .row-filter th').each(function()
			{
				$(this).html('<input type="text" class="form-control input-sm" placeholder="Search...">');
			});
			$('#datatable-column-filter .row-filter input').on('keyup change', function()
			{
				dtTable
					.column($(this).parent().index() + ':visible')
					.search(this.value)
					.draw();
			});
			// datatable with paging options and live search
			$('#featured-datatable').dataTable(
			{
				sDom: "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
			});
			// datatable with export feature
			var exportTable = $('#datatable-data-export').DataTable(
			{
				sDom: "T<'clearfix'>" +
					"<'row'<'col-sm-6'l><'col-sm-6'f>r>" +
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				"tableTools":
				{
					"sSwfPath": "assets/vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf"
				}
			});
			// datatable with scrolling
			$('#datatable-basic-scrolling').dataTable(
			{
				scrollY: "300px",
				scrollCollapse: true,
				paging: false
			});
		});
		</script>

	</body>

</html>
