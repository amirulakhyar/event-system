<?php
	session_start();

	if (!isset($_SESSION['scan2goA'])) {
		$_SESSION['msg'] = "You must log in first";
		header('location: login.php');
	}

	if (isset($_GET['logout'])) {
		session_destroy();
		unset($_SESSION['scan2goA']);
		header("location: login.php");
	}

$event_id = $_GET['id'];
if (empty($event_id)) { header("location:event.php"); }
include('database.php');
$sql = "SELECT * FROM `event` WHERE `event_id` = '$event_id' ";
$result = $db->query($sql);
if ($result->num_rows > 0) {}
else {header("location:event.php");}

?>
<!doctype html>
<html lang="en">

<head>
		<title>SCAN2 GO SYSTEM</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/vendor/themify-icons/css/themify-icons.css">
		<link rel="stylesheet" href="assets/vendor/pace/themes/orange/pace-theme-minimal.css">
		<link rel="stylesheet" href="assets/vendor/datatables/css-main/jquery.dataTables.min.css">
		<link rel="stylesheet" href="assets/vendor/datatables/css-bootstrap/dataTables.bootstrap.min.css">
		<link rel="stylesheet" href="assets/vendor/datatables-tabletools/css/dataTables.tableTools.css">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="assets/css/main.min.css">
		<link rel="stylesheet" href="assets/css/skins/sidebar-nav-darkgray.css" type="text/css">
		<link rel="stylesheet" href="assets/css/skins/navbar3.css" type="text/css">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	</head>
	<body>
		<!-- WRAPPER -->
		<div id="wrapper">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="brand">
					<a href="index.php">
						<img src="assets/img/logo-white.png" alt="Logo" class="img-responsive logo" width="150px;" style="margin-top:-15px;">
					</a>
				</div>
				<div class="container-fluid">
					<div id="tour-fullwidth" class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="ti-arrow-circle-left"></i></button>
					</div>
					<form class="navbar-form navbar-left search-form">
						<input type="text" value="" class="form-control" placeholder="Search dashboard...">
						<button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
					</form>
					<div id="navbar-menu">
						<ul class="nav navbar-nav navbar-right">

							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="assets/img/user.png" alt="Avatar">
									<span>Samuel</span>
								</a>
								<ul class="dropdown-menu logged-user-menu">
									<li><a href="#"><i class="ti-user"></i> <span>My Profile</span></a></li>
									<li><a href="appviews-inbox.html"><i class="ti-email"></i> <span>Message</span></a></li>
									<li><a href="#"><i class="ti-settings"></i> <span>Settings</span></a></li>
									<li><a href="page-lockscreen.html"><i class="ti-power-off"></i> <span>Logout</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- LEFT SIDEBAR -->
			<div id="sidebar-nav" class="sidebar">
				<nav>
					<ul class="nav" id="sidebar-nav-menu">
						<li class="menu-group">Main</li>
						<li><a href="index.php"><i class="ti-dashboard"></i> <span class="title">Dashboard</span></a></li>
						<li><a href="event.php"><i class="ti-crown"></i> <span class="title">Event</span></a></li>
						<li><a href="report.php"><i class="ti-zip"></i> <span class="title">Report</span></a></li>
						<li><a href="setting.php"><i class="ti-settings"></i> <span class="title">Setting</span></a></li>
						<li><a href="logout.php"><i class="ti-share-alt"></i> <span class="title">Sign Out</span></a></li>
					</ul>
					<button type="button" class="btn-toggle-minified" title="Toggle Minified Menu"><i class="ti-arrows-horizontal"></i></button>
				</nav>
			</div>
			<!-- END LEFT SIDEBAR -->
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<?php
					$sql = "SELECT * FROM `event` WHERE `event_id` = '$event_id' ";
					$result = $db->query($sql);
					if ($result->num_rows > 0) {
					    while($row = $result->fetch_assoc()) {
								$event_title = $row['event_title'];
								$event_date = $row['event_date'];
								$event_time = $row['event_time'];
								$event_author = $row['event_author'];
								$event_status =$row['event_status'];
								$event_info =$row['event_info'];
								$event_banner =$row['event_banner'];
					    }
					}
					if($event_banner != NULL){$banner="uploads/".$event_banner;}
					else{$banner="assets/img/profile-bg.png";}


					 ?>
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title"><?php echo $event_title; ?></h1>
						</div>
					</div>
					<div class="container-fluid">
						<div class="panel panel-profile" style="min-height:700px;">
							<div class="clearfix">
								<!-- LEFT COLUMN -->
								<div class="profile-left">
									<!-- PROFILE HEADER -->
									<div class="profile-header">
										<div class="overlay"></div>
										<div class="profile-main"style="height:200px; background-image:url('<?php echo $banner; ?>');">

										</div>
										<div class="profile-stat">
											<div class="row">
												<div class="col-md-4 stat-item">
													<?php echo $event_id; ?>
													<span>EVENT ID</span>
												</div>
												<div class="col-md-4 stat-item">
													<?php
													$no_att =0;
													$sql = "SELECT * FROM `participant` WHERE `participant_attandance` = '1' AND `participant_event_id` = '$event_id'";
													$result = $db->query($sql);
													if ($result->num_rows > 0) {
													    while($row = $result->fetch_assoc()) {
													       $no_att++;
													    }
													}
													echo $no_att;
													 ?>
													<span>ATTENDANCE</span>
												</div>
												<div class="col-md-4 stat-item">
													<?php
													$no_re =0;
													$sql = "SELECT * FROM `participant` WHERE `participant_event_id`= '$event_id'";
													$result = $db->query($sql);
													if ($result->num_rows > 0) {
															while($row = $result->fetch_assoc()) {
																 $no_re++;
															}
													}
													echo $no_re;
													 ?>
													<span>REGISTER</span>
												</div>
											</div>
										</div>
									</div>
									<!-- END PROFILE HEADER -->
									<!-- PROFILE DETAIL -->
									<div class="profile-detail">
										<div class="profile-info">
											<h4 class="heading">Basic Info</h4>
											<ul class="list-unstyled list-justify">
												<li>Title
													<span><?php echo $event_title ?></span>
												</li>
												<li>Date
													<span><?php echo $event_date ?></span>
												</li>
												<li>Time
													<span><?php echo $event_time ?></span>
												</li>
												<li>Author
													<span><?php echo $event_author ?></span>
												</li>
											</ul>
										</div>

										<div class="profile-info">
											<h4 class="heading">About</h4>
											<p><?php echo $event_info ?></p>
										</div>

										<div class="text-center"><a href="action/delete_event.php?id=<?php echo $event_id;?>" class="btn btn-danger">Delete Event</a></div> <br>
									</div>
									<!-- END PROFILE DETAIL -->
								</div>
								<!-- END LEFT COLUMN -->
								<!-- RIGHT COLUMN -->
								<div class="profile-right">
									<h4 class="heading"><?php echo $event_title; ?></h4>
									<h5>ATTENDANCE</h5>
									<table id="datatable-basic-scrolling" class="table table-sorting table-hover datatable">
										<thead>
											<tr>
												<th>Name</th>
												<th>Ticket ID</th>
												<th>Time</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$sql = "SELECT * FROM `participant` WHERE `participant_attandance` = '1' AND `participant_event_id` = '$event_id'";
											$result = $db->query($sql);
											if ($result->num_rows > 0) {
											    while($row = $result->fetch_assoc()) {
											        echo "
															<tr>
																<th>" . $row["participant_name"]. "</th>
																<th>" . $row["participant_code"]. "</th>
																<th>" . $row["participant_attandance_time"]. "</th>
															</tr>
															";
											    }
											}
											 ?>
										</tbody>
									</table>
									<h5>REGISTER</h5>
									<table id="datatable-basic-scrolling1" class="table table-sorting table-hover datatable">
										<thead>
											<tr>
												<th>Name</th>
												<th>IC</th>
												<th>No. Matrik</th>
												<th>No. Phone</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$sql = "SELECT * FROM `participant` WHERE `participant_event_id`= '$event_id'";
												$result = $db->query($sql);

												if ($result->num_rows > 0) {
													while($row = $result->fetch_assoc()) {
													echo "
													<tr>
														<td>".$row["participant_name"]."</td>
														<td>".$row["participant_ic"]."</td>
														<td>".$row["participant_matick"]."</td>
														<td>".$row["participant_no"]."</td>
														<td>
															<a href='https://wa.me/6".$row["participant_no"]."?text=QR:%20https://api.qrserver.com/v1/create-qr-code/?data=".$row["participant_code"]."%0AName:%20".$row["participant_name"]."%0AEvent%20Name:%20".$event_title."%0ATime:%20".$event_time."%0AAuthor:%20".$event_author."%0A

'><button type='button' class='btn btn-success btn-outline'><i class='fa fa-whatsapp' aria-hidden='true'></i></button></a>
															<a href='action/delete_register.php?id=".$row["participant_id"]."'><button type='button' class='btn btn-danger btn-outline'><i class='fa fa-trash' aria-hidden='true'></i></button></a>
														</td>
													</tr>
															";
													}
												}
											?>
										</tbody>
									</table>
									<h5>AJK</h5>
									<table id="datatable-basic-scrolling2" class="table table-sorting table-hover datatable">
										<thead>
											<tr>
												<th>Name</th>
												<th>Username</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$sql = "SELECT * FROM `ajk` WHERE `ajk_event_id`='$event_id'";
											$result = $db->query($sql);
											if ($result->num_rows > 0) {
											    while($row = $result->fetch_assoc()) {
											        echo "
															<tr>
																<td>" . $row["ajk_name"]. "</td>
																<td>" . $row["ajk_username"]. "</td>
																<td><a href='action/delete_ajk.php?id=".$row["ajk_id"]."'><button type='button' class='btn btn-danger btn-outline'><i class='fa fa-trash' aria-hidden='true'></i></button></a></td>
															</tr>


															";
											    }
											}


											 ?>
										</tbody>
									</table>
								</div>
								<!-- END RIGHT COLUMN -->
							</div>
						</div>
					</div>
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">REGISTER</h3>
									</div>
									<div class="panel-body">
										<form class="form-horizontal" action="action/register.php" method="post">
											<div class="form-group">
												<label class="col-sm-2 control-label">Name :</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase();" name="name" required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">No IC :</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" name="ic" required>
												</div>
											</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">No Phone :</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" name="no" required>
											</div>
										</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">No Matric :</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" name="matric" required>
													<input type="text" name="event" value="<?php echo $event_id ;?>"style="display:none;" required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"></label>
												<div class="col-sm-10">
													<button type="submit" class="btn btn-primary btn-sm btn-block">Submit</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">AJK</h3>
									</div>
									<div class="panel-body">
										<form class="form-horizontal" action="action/create_ajk.php" method="post">
											<div class="form-group">
												<label class="col-sm-2 control-label"> Name :</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" name="name" onkeyup="this.value = this.value.toUpperCase();" required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"> Username :</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" name="username" required>
													<input type="text" name="event" value="<?php echo $event_id ;?>"style="display:none;" required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Password  :</label>
												<div class="col-sm-10">
													<input type="password" class="form-control" name="pass"  required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"></label>
												<div class="col-sm-10">
													<button type="submit" class="btn btn-primary btn-sm btn-block">Submit</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">SETTING</h3>
									</div>
									<div class="panel-body">
										<form class="form-horizontal" action="action/upload_banner.php" method="POST" enctype="multipart/form-data">
											<div class="form-group">
												<label class="col-sm-2 control-label">Banner  :</label>
												<input type="text" name="event" value="<?php echo $event_id ;?>"style="display:none;" required>
												<div class="col-sm-10">
													<input type="file" name="file"class="form-control"  required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"></label>
												<div class="col-sm-10">
													<button type="submit" class="btn btn-primary btn-sm btn-block">Submit</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->
			<div class="clearfix"></div>
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; 2020 <a href="" target="_blank">SCAN2GO	</a></p>
				</div>
			</footer>
		</div>
		<!-- END WRAPPER -->
		<!-- Javascript -->
		<script src="assets/vendor/jquery/jquery.min.js"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/vendor/pace/pace.min.js"></script>
		<script src="assets/scripts/klorofilpro-common.min.js"></script>
		<script src="assets/vendor/datatables/js-main/jquery.dataTables.min.js"></script>
		<script src="assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js"></script>
		<script src="assets/vendor/datatables-colreorder/dataTables.colReorder.js"></script>
		<script src="assets/vendor/datatables-tabletools/js/dataTables.tableTools.js"></script>
		<script>
		$(function()
		{
			// datatable column with reorder extension
			$('#datatable-column-reorder').dataTable(
			{
				pagingType: "full_numbers",
				sDom: "RC" +
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				colReorder: true,
			});
			// datatable with column filter enabled
			var dtTable = $('#datatable-column-filter').DataTable(
			{ // use DataTable, not dataTable
				sDom: // redefine sDom without lengthChange and default search box
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>"
			});
			$('#datatable-column-filter thead').append('<tr class="row-filter"><th></th><th></th><th></th><th></th><th></th></tr>');
			$('#datatable-column-filter thead .row-filter th').each(function()
			{
				$(this).html('<input type="text" class="form-control input-sm" placeholder="Search...">');
			});
			$('#datatable-column-filter .row-filter input').on('keyup change', function()
			{
				dtTable
					.column($(this).parent().index() + ':visible')
					.search(this.value)
					.draw();
			});
			// datatable with paging options and live search
			$('#featured-datatable').dataTable(
			{
				sDom: "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
			});
			// datatable with export feature
			var exportTable = $('#datatable-data-export').DataTable(
			{
				sDom: "T<'clearfix'>" +
					"<'row'<'col-sm-6'l><'col-sm-6'f>r>" +
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				"tableTools":
				{
					"sSwfPath": "assets/vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf"
				}
			});
			// datatable with scrolling
			$('#datatable-basic-scrolling').dataTable(
			{
				scrollY: "300px",
				scrollCollapse: true,
				paging: false
			});
			$('#datatable-basic-scrolling1').dataTable(
			{
				scrollY: "300px",
				scrollCollapse: true,
				paging: false
			});
			$('#datatable-basic-scrolling2').dataTable(
			{
				scrollY: "300px",
				scrollCollapse: true,
				paging: false
			});
		});

		</script>

	</body>

</html>
